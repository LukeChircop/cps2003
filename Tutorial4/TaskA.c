#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>

 
void main(int argc, char *argv[]) //Producer
{
	int shmid;
	key_t key;
	size_t size;
	char *shm;
	int *buffer;
	int counter = 0;

	// Shared memory segment identifier
	key = 0x1234;
	size = 1024 * 8;

	if(argc < 2)
	{
		perror("Arguments!");
		exit(1);
	}

	printf("Producer Started\n");
	// Create the segment.
	if ((shmid = shmget(key, size, IPC_CREAT | 0666)) < 0) 
	{
  		perror("shmget");
  		exit(1);
	}

	// Attach to the segment.
	if ((shm = shmat(shmid, NULL, 0)) == (char *) -1) 
	{
	 	perror("shmat");
	  	exit(1);
	}

	buffer = (int*)shm;

	while(counter < 10)
	{
		for (int index = 0; index < 10; ++index)
    		printf(" %d ",buffer[index]);
    	
    	printf("\n");

    	counter++;

    	sleep(atoi(argv[1]));
	}

	printf("Producer Ended\n");

	if(shmctl(shmid, IPC_RMID ,NULL) == -1)
	{
		perror("shmctl");
		exit(1);
	}
// shm is now a pointer to an 8kB shared memory 
}