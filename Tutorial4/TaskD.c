#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>

int shmid;
key_t key;
size_t size;
char *shm;

    
// Find the segment (note that since we're trying to
// locate the segment, we no longer use the IPC_CREAT 
// flag when we call shmget)
void main(int argc, char *argv[]) //Consumer
{

	char *buffer;

	if(argc <3){		
		perror("Arguments!");
		exit(1);
	}

	FILE *fp = fopen(argv[2],"w");

	// Shared memory segment identifier
	key = 0x1234;
	size = 1024 * 8;

	printf("Consumer Started\n");

	if ((shmid = shmget(key, size, 0666)) < 0) 
	{
	  perror("shmget");
	  exit(1);
	}

	// Attach to the segment.
	if ((shm = shmat(shmid, NULL, 0)) == (char *) -1) 
	{
	  perror("shmat");
	  exit(1);
	}

	buffer = (char*)shm;

	while(buffer[0] != '&')
	{
		fputs(buffer,fp);	

    	printf("Updated Buffer \n");
    	fflush(stdout);

    	sleep(atoi(argv[1]));
	}
	
	fclose(fp);

	printf("Consumer Ended\n");

	shmdt(shm);	
// shm is now a pointer to an 8kB shared memory created by producer
}