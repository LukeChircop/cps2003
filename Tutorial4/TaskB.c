#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>

int shmid;
key_t key;
size_t size;
char *shm;

int current_index = 0;
    
// Find the segment (note that since we're trying to
// locate the segment, we no longer use the IPC_CREAT 
// flag when we call shmget)
void main(int argc, char *argv[]) //Consumer
{

	int *buffer;
	int counter =0;

	// Shared memory segment identifier
	key = 0x1234;
	size = 1024 * 8;

	if(argc < 2)
	{
		perror("Arguments!");
		exit(1);
	}

	printf("Consumer Started\n");

	if ((shmid = shmget(key, size, 0666)) < 0) 
	{
	  perror("shmget");
	  exit(1);
	}

	// Attach to the segment.
	if ((shm = shmat(shmid, NULL, 0)) == (char *) -1) 
	{
	  perror("shmat");
	  exit(1);
	}

	buffer = (int*)shm;

	while(counter < 10)
	{
		for (int index = 0; index < 10; ++index)
    		buffer[index] = ++current_index;

    	printf("Updated Buffer \n");
    	fflush(stdout);

    	counter++;

    	sleep(atoi(argv[1]));
	}
	
	printf("Consumer Ended\n");

	shmdt(shm);	
// shm is now a pointer to an 8kB shared memory created by producer
}