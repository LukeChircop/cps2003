#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>

 
void main(int argc, char *argv[]) //Producer
{
	int shmid;
	key_t key;
	size_t size;
	char *shm;
	char *buffer;
	size_t nread;

	if(argc < 3)
	{
		perror("Arguments!");
		exit(1);
	}

	FILE *fp = fopen(argv[2],"r");

	// Shared memory segment identifier
	key = 0x1234;
	size = 1024 * 8;

	printf("Producer Started\n");
	// Create the segment.
	if ((shmid = shmget(key, size, IPC_CREAT | 0666)) < 0) 
	{
  		perror("shmget");
  		exit(1);
	}

	// Attach to the segment.
	if ((shm = shmat(shmid, NULL, 0)) == (char *) -1) 
	{
	 	perror("shmat");
	  	exit(1);
	}


	buffer = (char*)shm;


	if (fp) {
	    while (fread(buffer, 1, size, fp) > 0)
	    {	    	
	    	printf("%s \n", buffer);
	    	sleep(atoi(argv[1]));
	    }

	    
	    if (ferror(fp)) {
	        /* deal with error */
	    }

		buffer[0] = '&';

	    fclose(fp);
	}


	printf("Producer Ended\n");

	if(shmctl(shmid, IPC_RMID ,NULL) == -1)
	{
		perror("shmctl");
		exit(1);
	}
}