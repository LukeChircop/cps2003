#!/bin/bash
# CPS2003 - Systems programming

# Piping - sending data from one program to another by feeding the output from the program on the left as input to the program on the right.

# Operator used is  '|'

echo
echo Piping example for ls, head -3
echo

ls | head -3

echo
echo Another piping example ls, head -3, tail -1
echo

ls | head -3 | tail -1


#Other useful commands:

# >  - Save output to a file.
# >>  - Append output to a file.
# <  - Read input from a file.
# 2>  - Redirect error messages.
