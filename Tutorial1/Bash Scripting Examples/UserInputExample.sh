#!/bin/bash
# CPS2003 - Systems programming

echo Hello, who am I talking to?

read varname

echo It\'s nice to meet you $varname

# You can include more than one variable for the read command 

read -p 'Username: ' uservar
read -sp 'Password: ' passvar

echo
echo Thankyou $uservar we now have your login details
