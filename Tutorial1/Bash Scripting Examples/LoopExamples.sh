#!/bin/bash
# Basic while loop

echo
echo Starting While loop
echo
counter=1

while [ $counter -le 10 ]
do
	echo $counter
	((counter++))
done

echo
echo All done
echo

echo Starting for loop
echo

for value in {1..10}
do
	echo $value
done

echo
echo All done


#break

#continue
