#!/bin/bash
# Passing arguments to a function

print_something () {
	echo Hello $1
}

print_something Mars
print_something Jupiter

echo
echo Functions with return
echo

print_something_else () {
	echo Hello $1
	return 5
}

print_something_else Mars
print_something_else Jupiter

echo The previous function has a return value of $?
echo

echo All done
