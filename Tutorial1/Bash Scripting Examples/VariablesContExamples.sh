#!/bin/bash
# CPS2003 - Systems programming

# We can also set our own variables

myName=Luke

mySurname=Skywalker

echo Hello $myName $mySurname

# Notice there is no space between the name of the variable, '=' and value of variable.

# To store variable values containing spaces we use quotes. 

sentence="Hello I am stored in one variable using quotes!"

echo $sentence

#We can also take the output of a command or program and save it as the value of a variable 

directoryList=$( ls )

echo Command Substitution ( Output for ls command ): $directoryList

