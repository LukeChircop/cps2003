#include <stdlib.h>
#include <stdio.h>
#include <curses.h>
 
 
int main(void) {
 
    WINDOW * mainwin, * childwin;

    int      ch;
 
    int      width = 35, height = 7;
    int      rows  = 25, cols   = 80;

    /* Find center of current terminal window */
    int      x = (cols - width)  / 2;
    int      y = (rows - height) / 2;
 
 
    /*  Initialize ncurses  */
 
    if ( (mainwin = initscr()) == NULL ) {
	fprintf(stderr, "Error initialising ncurses.\n");
	exit(EXIT_FAILURE);
    }
 
    /*  Switch of echoing */

    noecho();

    /*  Make our child window, and add
	a border and some text to it.   */
 
    childwin = subwin(mainwin, height, width, y, x);
    box(childwin, 0, 0);
    mvwaddstr(childwin, 1, 2, "Press R: change colour to RED ");
    mvwaddstr(childwin, 2, 2, "Press B: change colour to BLUE  ");
    mvwaddstr(childwin, 3, 2, "Press G: change colour to GREEN");
    mvwaddstr(childwin, 5, 10, "Press 'q' to quit");
 	
    start_color();
    init_pair(1, COLOR_BLACK, COLOR_RED);
    init_pair(2, COLOR_BLACK, COLOR_BLUE);
    init_pair(3, COLOR_BLACK, COLOR_GREEN);

    refresh();
 
    /*  Loop until user hits 'q' to quit  */
 
    while ( (ch = getch()) != 'q' ) {

		switch ( ch ) {
 
			case 'r':
			wbkgd(childwin,COLOR_PAIR(1));
		    break;
		
	 
		    case 'b':
		    wbkgd(childwin,COLOR_PAIR(2));
		    break;
		    

		    case 'g':
		    wbkgd(childwin,COLOR_PAIR(3));
		    break;
	    }

		mvwin(childwin, y, x);
	    wnoutrefresh(childwin);
	    doupdate();
    }
 
 
    /*  Clean up after ourselves  */
 
    delwin(childwin);
    delwin(mainwin);
    endwin();
    refresh();
 
    return EXIT_SUCCESS;
}
