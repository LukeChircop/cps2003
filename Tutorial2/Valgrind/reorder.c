#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

#define BSZ 128       /* input buffer size */
#define MAXLINE 100000     /* lines stored */
char *lines[MAXLINE];
char buf[32];    /* BUG FIX #2: change 32 to BSZ */
int nlines;

char *
append(const char *str1, const char *str2)
{
  int n = strlen(str1) + strlen(str2) + 1;
  char *p = malloc(n);

  if (p != NULL) {
    strcpy(p, str1); 
    strcat(p, str2);
  }
  return(p);
}

void
printrandom()
{
  long idx = random() % nlines;
  fputs(lines[idx], stdout);
 //free(lines[idx]);   //BUG FIX #3: add this line */
  lines[idx] = lines[--nlines];
}

int
main()
{
  int n, doappend;
  char *str, *ostr;

   close(0);   /* BUG FIX #1: remove this line */
  srandom(time(NULL));
  nlines = 0;
  doappend = 0;
  str = NULL;
  while (fgets(buf, BSZ, stdin) != NULL) {
    n = strlen(buf);
    if (n == BSZ-1 && buf[BSZ-2] != '\n')
      doappend = 1;
    else
      doappend = 0;
    if (str != NULL) {
      ostr = str;
      str = append(ostr, buf);
      while (str == NULL) {
        printrandom();
        str = append(ostr, buf);
      }
      free(ostr);
    } else {
      str = strdup(buf);
      while (str == NULL) {
        printrandom();
        str = strdup(buf);
      }
    }
    if (!doappend) {
      if (nlines == MAXLINE)
        printrandom();
    lines[nlines++] = str;
    str = NULL;
    }
  }
  while (nlines != 0)
    printrandom();
  exit(0);
}
