/* A simple server in the internet domain using TCP
   The port number is passed as an argument */
//Taken from: http://www.linuxhowtos.org/C_C++/socket.htm

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

int main(int argc, char *argv[])
{
     int sockfd, newsockfd, portno;
     socklen_t clilen;
     char buffer[256];
     struct sockaddr_in serv_addr, cli_addr;
     int n;
     if (argc < 2) {
         fprintf(stderr,"ERROR, no port provided\n");
         exit(1);
     }
     
     //Creates an endpoint for communication and returns a descriptor
     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) 
        error("ERROR opening socket");
        
     //Sets first n bytes of the area to zero
     bzero((char *) &serv_addr, sizeof(serv_addr));
     //Converting initial portion of string to integer
     portno = atoi(argv[1]);     
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     //Converting unsigned short integer from host byte order to network byte order. 
     serv_addr.sin_port = htons(portno);
     
     //Assigning address specified by addr to the socket referred by the sockfd
     if (bind(sockfd, (struct sockaddr *) &serv_addr,
              sizeof(serv_addr)) < 0) 
              error("ERROR on binding");
            
     //Marking socket as a socket that will be used to accept incoming connection requests  
     listen(sockfd,5);
     clilen = sizeof(cli_addr);
     
     //Accepting an incoming connection request
     newsockfd = accept(sockfd, 
                 (struct sockaddr *) &cli_addr, 
                 &clilen);
     if (newsockfd < 0) 
          error("ERROR on accept");
          
    //Read Picture Size
    printf("Reading Picture Size\n");
    int size;
    read(newsockfd, &size, sizeof(int));

    //Read Picture Byte Array
    printf("Reading Picture Byte Array\n");
    char p_array[size];
    read(newsockfd, p_array, size);

    //Convert it Back into Picture
    printf("Converting Byte Array to Picture\n");
    FILE *image;
    image = fopen("received_lena.png", "w");
    fwrite(p_array, 1, sizeof(p_array), image);
    fclose(image);
     
     //Closing the sockets opened 
     close(newsockfd);
     close(sockfd);
     return 0; 
}
