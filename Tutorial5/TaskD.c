#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>

void list_dir(char * dir_name, int flag){
        
    struct dirent *dir_ent;

    DIR *d_stream = opendir( dir_name );

    while((dir_ent = readdir(d_stream) )!= NULL){
        
        if (dir_ent->d_type == DT_DIR)
        {
            printf("D = %s\n", dir_ent->d_name );
            if ((flag==1) && (strcmp(dir_ent->d_name,".") != 0) && (strcmp(dir_ent->d_name,"..") != 0)){

                char String[1024];
                sprintf(String, "%s/%s", dir_name, dir_ent->d_name);
                list_dir(String, 1);
            }
        }else
            printf("F = %s\n", dir_ent->d_name );
    }

    closedir(d_stream);
}

int main(int argc, char *argv[])
{

    if (argc < 2) {
       fprintf(stderr,"usage %s directory_name -R \n", argv[0]);
       exit(0);
    }

    if (argc == 3 && (strcmp(argv[2],"-R") == 0))
    {
        list_dir(argv[1],1);
    }
    else
    {
        list_dir(argv[1],0);
    }

	return 0;
}
