#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/stat.h>

int main(int argc, char *argv[])
{
	int size = 1024;
	int count =0;

    if (argc < 3) {
       fprintf(stderr,"usage %s file_name destination_name\n", argv[0]);
       exit(0);
    }

	// Fork process returns 0 for the child and the process id of child process to the parent

	pid_t pid = fork();

	
	if (pid < 0) // Has the fork method failed?
	{
		perror("fork failed!");
	}

	else if (pid == 0) // For child process, pid = 0
	{ 
		char String[255];
		sprintf(String, "./TaskA %s > %s", argv[1], argv[2]);
        system(String);
		_exit(0);
	}
	else // For parent process, pid > 0
	{
		wait(NULL);
		struct stat st;
		stat(argv[1], &st);
		size = st.st_size;
		printf("Program finished, the file size is %d Bytes\n",size);
	}
	return 0;
}
