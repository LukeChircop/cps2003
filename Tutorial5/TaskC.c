#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <dirent.h>

int main(int argc, char *argv[])
{

    if (argc < 2) {
       fprintf(stderr,"usage %s directory_name \n", argv[0]);
       exit(0);
    }

    struct dirent *dir_ent;

    DIR *d_stream = opendir( argv[1] );

    while((dir_ent = readdir(d_stream) )!= NULL){
    	
    	if (dir_ent->d_type == DT_DIR)
    	{
    		printf("D = %s\n", dir_ent->d_name );
    	}else
    		printf("F = %s\n", dir_ent->d_name );
    }

    closedir(d_stream);
	return 0;
}
