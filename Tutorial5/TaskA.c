#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	int size = 1024;
	int count =0;

    if (argc < 2) {
       fprintf(stderr,"usage %s file name\n", argv[0]);
       exit(0);
    }

    FILE *text = fopen(argv[1], "r");

    char send_buffer[size];
    while(!feof(text)) {
    	count = fread(send_buffer, 1, size, text);
    	for(int i = 0 ; i < count ; i++){
      		printf("%c", send_buffer[i]);    		
    	}
    	printf("\n");
        fflush(stdout);
    }
}