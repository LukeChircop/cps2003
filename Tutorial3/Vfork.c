#include<stdio.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>

int main(void)
{

	int counter = 0;

	//vfork allows the child and parent to share the same address space

	pid_t pid = vfork();

	if(pid < 0)
	{
		perror("Operation failed!");
	}
	else if(pid == 0)
	{
		for(counter = 0; counter < 50; counter++);
		_exit(0);
	}
	else
	{
		wait(NULL);
		printf("Parent : %d \n", counter);
	}
	return 0;
}