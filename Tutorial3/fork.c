#include<stdio.h>
#include<unistd.h>
#include<sys/wait.h>

void PrintChild(void)
{
	printf("This is a child process. \n");
}

void PrintParent(void)
{
	printf("This is the parent process. \n"); 
}


/*
 *The fork creates a new process 
 *Difference between process and program?
 */

int main(void)
{
	printf("Parent process... \n");

	// Fork process returns 0 for the child and the process id of child process to the parent

	pid_t pid = fork();

	
	if (pid < 0) // Has the fork method failed?
	{
		perror("fork failed!");
	}

	else if (pid == 0) // For child process, pid = 0
	{ 
		pid_t child_pid = getpid();
		PrintChild();
		printf("Child process [pid=%d]\n", child_pid);
		_exit(0);
	}
	else // For parent process, pid > 0
	{
		PrintParent();
		wait(NULL);
		printf("Parent process, [child pid=%d]\n", pid);
	}
	return 0;
}
