#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>

int main(void)
{
	printf("This example will be executing a system call");

	//Call System functions
	system("./fork.out");

	//Call exec to initiate a new program
	execl("./fork.out","", NULL);

	return 0;
}
