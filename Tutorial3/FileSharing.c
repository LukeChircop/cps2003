#include<stdio.h>
#include<unistd.h>
#include<sys/types.h>
#include <sys/wait.h>

/*
 * Filedescriptors are inherited by the child processes thus shared by the parent and child
 */

int main(void)
{
    FILE *f = fopen("shared_file.txt", "w");

    char* parentSpeech = "Process! ... I am your father! \n";
    char* childSpeech = "NOOOOOOOOO!!! \n";

    if(f == NULL)
    {
        printf("File could not be opened \n");
        return 0;
    }
    else
    {
        pid_t pid = fork();

        if(pid  < 0)
        {
            perror("Fork failed !");
        }
        else if(pid == 0)
        {
            fputs(childSpeech, f);
            printf("Child process denied his father \n");
            _exit(0);
        }
        else
        {
            fputs(parentSpeech, f);
            wait(NULL);
            printf("Parent process ready \n");
        }
    }
    return 0;
}
